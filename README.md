# My exercise is aiming to configurate Web development environment using Vagrant, Ansible, Docker.
## Generally, we will set up an environment which have 3 servers:
### - CI server: Where consists of cripts in order to set up the environment for other servers and include scripts to deploy source code. CI server will be able to connect to others
### - Web server: in where we will build two containers consisting php and web server nginx via Docker-compose
### - DB server: we will build Mysql container. By accessing this server successfully, application will show a message "Connection Successful"
## Let's follow me step by step
## 1. Clone Source from my gitlab
``` 
$ git clone https://gitlab.com/quoctinle/install_config_server_automation.git 
```
### when you finish cloning my repository, you will have to create VMs
## 2. Start creating VMs, run this command: 
``` 
$ vagrant up 
```
### The results of the process will show that we already had three VMs: Ci, web, db
## 3. Download roles
### I already created the file named requirements.yml, let's install roles by following commands
``` $vagrant ssh ci
    $cd ansible-playbook/
    $ansible-galaxy install -p roles -r requirements.yml
```    
## 4. Config the environment for web, db server. 
###    You have two files: local.host which consists of ip addresses of three servers and provision.yml which aims to install and configurate the enviroment with roles for servers: web, db.
###    Let's run command:
```
$ vagrant ssh ci
$ cd ansible-playbook/
$ ansible-playbook -i local.host provision.yml
```
### the process will take a few time to run tasks. If you have any trouble such as "cannot-retrieve-metalink-for-repo-epel", let's ssh to the server occurred errors and run this command:
```
$ sudo sed -i "s/mirrorlist=https/mirrorlist=http/" /etc/yum.repos.d/epel.repo
```
## 5. Build containers through Docker-compose
### - Firstly, Let's SSH to Web machine and run the commands below to create 2 containers including php, nginx
```
$ vagrant ssh web
$ cd ansible-playbook/
$ docker-compose -f docker-compose.yml up -d
```
### - Secondly, Let's SSH to DB machine and do the same thing 
```
$ vagrant ssh db
$ cd ansible-playbook/
$ docker-compose -f docker-compose-mysql.yml up -d
```
### - Finally, To check the existing containers, let's type:
```
$ sudo docker ps 
```
## 6. Run file .php to connect to Db server:
### We already set up file .php in public folder
```
<?php
$servername   = "192.168.33.32";
$database = "db";
$username = "user";
$password = "password";
// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
   die("Connection failed: " . $conn->connect_error);
}
  echo "Connected successfully";
?>
```
### Connect to this address: 192.168.33.31:9090/condb.php and see your result.
### Hope you have a nice day!


